# LIFX button ESP8266
A standard button to toggle all LIFX bulbs on the network.

Logs WiFi connection phase, calibration and usage to serial monitor for debugging.

## Secrets.h
To setup a connection, ensure you create a `secrets.h` file. See `secrets EXAMPLE.h` for an example.

## Demo video
To cover the existing non-smart switch, I constructed a wooden housing for the arduino/button. Click to picture to view my demo video:
[![YouTube video link](http://img.youtube.com/vi/16etzad09g4/0.jpg)](http://www.youtube.com/watch?v=16etzad09g4 "DIY WiFi LIFX Light Switch (No neutral wire or capacitor)")
