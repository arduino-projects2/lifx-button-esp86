#include <ESP8266WiFi.h>

#include "LIFX.h"
#include "secrets.h"

// Initialize the client library
WiFiClient client;

const unsigned int BUTTON_PIN = 13;
unsigned int localPort = 8888; // Port to listen for responses on

bool isLightOn = false;
unsigned int lastLightCheck = 0;
const unsigned int lightCheckFrequency = 3000; // Check light status every 3 seconds

void setup()
{
	Serial.begin(9600);

	pinMode(BUTTON_PIN, INPUT_PULLUP);
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, HIGH);
	wifiSetUp();
	Udp.begin(localPort); // Listen for incoming UDP packets
}

bool buttonDown = false;
uint8_t dest[] = {0xd0, 0x73, 0xd5, 0x62, 0xcb, 0x18}; // broadcast MAC

void loop()
{
	if (millis() - lastLightCheck > lightCheckFrequency)
	{
		lastLightCheck = millis();

		const uint16_t power = getPower(dest);
		if (power == -1)
		{
			Serial.print("ERROR OCCURED WHEN GETTING LIGHT STATE!");
		}
		else
		{
			isLightOn = power > 0;
			Serial.print("Polled light status: ");
			Serial.println(isLightOn);
		}
	}

	const int buttonActive = digitalRead(BUTTON_PIN);
	if (buttonActive == LOW)
	{
		digitalWrite(LED_BUILTIN, LOW);

		if (!buttonDown)
		{
			buttonPressed();
		}
		buttonDown = true;
	}
	else
	{
		digitalWrite(LED_BUILTIN, HIGH);

		if (buttonDown)
		{
			buttonReleased();
		}
		buttonDown = false;
	}

	delay(10); // arbitrary delay to limit data to serial port
}

int unsigned lastPress = 0;
const int unsigned buttonCooldown = 50;
void buttonPressed()
{
	if (millis() - lastPress < buttonCooldown)
	{
		return;
	}
	Serial.println("Button pressed!");
	lastPress = millis();

	// Toggle light and update cached status
	setPower(dest, !isLightOn);
	isLightOn = !isLightOn;
}

void buttonReleased() {}

void wifiSetUp()
{
	Serial.print("Connecting to wifi...");

	// Set WiFi to station mode and disconnect from an AP if it was previously connected
	WiFi.disconnect();
	delay(1000);
	WiFi.mode(WIFI_STA);
	WiFi.setAutoConnect(true);
	WiFi.begin(ssid, pass); // Try to connect

	int count = 0;
	while (WiFi.status() != WL_CONNECTED && count < 10)
	{
		Serial.print(".");
		count++;
		delay(1000);
	}

	if (WiFi.status() == WL_CONNECTED)
	{
		delay(500);
		Serial.print("\r\nConnected, IP address: ");
		Serial.println(WiFi.localIP());
	}
	else
	{
		Serial.println("Failed!");
		delay(1000);
		wifiSetUp(); // Retry
	}
}
