#include <ESP8266WiFi.h>
#include <WiFiUdp.h>


// Settings
const int changeDuration = 750; // Time taken for bulb colour/brightness/state transitions in ms
const int timeoutInterval = 3000; // How to wait for messages from bulb


// The LIFX Header structure
#pragma pack(push, 1)
typedef struct {
  /* frame */
  uint16_t size;
  uint16_t protocol:12;
  uint8_t  addressable:1;
  uint8_t  tagged:1;
  uint8_t  origin:2;
  uint32_t source;
  /* frame address */
  uint8_t  target[8];
  uint8_t  reserved[6];
  uint8_t  res_required:1;
  uint8_t  ack_required:1;
  uint8_t  :6;
  uint8_t  sequence;
  /* protocol header */
  uint64_t :64;
  uint16_t type;
  uint16_t :16;
  /* variable length payload follows */
} lifx_header;
#pragma pack(pop)

// Device::SetPower Payload
#pragma pack(push, 1)
typedef struct {
  uint16_t level;
} lifx_payload_device_set_power;
#pragma pack(pop)

// Device::StatePower Payload
#pragma pack(push, 1)
typedef struct {
  uint16_t level;
} lifx_payload_device_state_power;
#pragma pack(pop)

// Payload types
#define LIFX_DEVICE_GETPOWER 20
#define LIFX_DEVICE_SETPOWER 21
#define LIFX_DEVICE_STATEPOWER 22

// Packet buffer size
#define LIFX_INCOMING_PACKET_BUFFER_LEN 300

// Length in bytes of serial numbers
#define LIFX_SERIAL_LEN 6

const unsigned int lifxPort  = 56700; // Port used to send to send to LIFX devices

// Remote IP (In this case we broadcast to the entire subnet)
IPAddress broadcast_ip(255, 255, 255, 255);
// IPAddress broadcast_ip(192, 168, 0, 49); // Bulb IP
// char targetMac[] = "d073d562cb18";    // broadcast MAC

// Packet buffer size
#define LIFX_INCOMING_PACKET_BUFFER_LEN 300

// An EthernetUDP instance to let us send and receive packets over UDP
WiFiUDP Udp;



// Recieves light on/off state as boolean
// Returns 0 if off, 65535 if on and -1 if an error occurs
uint16_t getPower(uint8_t *dest) {
  uint16_t power = 0;
  lifx_header header;
  
  // Initialise both structures
  memset(&header, 0, sizeof(header));

  // Set the target the nice way
  memcpy(header.target, dest, sizeof(uint8_t) * LIFX_SERIAL_LEN);

  // Setup the header
  header.size = sizeof(lifx_header); // Size of header + payload
  header.tagged = 0;
  header.addressable = 1;
  header.protocol = 1024;
  header.source = 123;
  // header.target = {0xD0, 0x73, 0xD5, 0x62, 0xCB, 0x18, 0x00, 0x00};
  header.ack_required = 0;
  header.res_required = 1;
  header.sequence = 100;
  header.type = LIFX_DEVICE_GETPOWER;

  
  // Send a packet on startup
  Udp.beginPacket(broadcast_ip, 56700);
  Udp.write((char *) &header, sizeof(lifx_header));
  Udp.endPacket();
  
  unsigned long started = millis();
  while (millis() - started < timeoutInterval) {
    int packetLen = Udp.parsePacket();
    byte packetBuffer[LIFX_INCOMING_PACKET_BUFFER_LEN];
    if (packetLen && packetLen < LIFX_INCOMING_PACKET_BUFFER_LEN) {
      Udp.read(packetBuffer, sizeof(packetBuffer));
      
      if (((lifx_header *)packetBuffer)->type == LIFX_DEVICE_STATEPOWER) {
        power = ((lifx_payload_device_state_power *)(packetBuffer + sizeof(lifx_header)))->level;
        return power;
      } else {
        Serial.print("Unexpected Packet type: ");
        Serial.println(((lifx_header *)packetBuffer)->type);
      }
    }
  }

  Serial.println("TIMED OUT");
  return -1;
}

// Sets light to on or off
void setPower(uint8_t *dest, bool bulbOn) {
	lifx_header header;
	lifx_payload_device_set_power payload;

	// Initialise both structures
	memset(&header, 0, sizeof(header));

  // Set the target the nice way
  memcpy(header.target, dest, sizeof(uint8_t) * LIFX_SERIAL_LEN);


	memset(&payload, 0, sizeof(payload));

	// Setup the header
	header.size = sizeof(lifx_header) + sizeof(payload); // Size of header + payload
	header.tagged = 0;
	header.addressable = 1;
	header.protocol = 1024; // LIFX protocol number
	header.source = 123;
	header.ack_required = 0;
	header.sequence = 100;
	header.type = LIFX_DEVICE_SETPOWER;

	// Setup payload
	payload.level = bulbOn ? 65535 : 0;

	// Send packet
	Udp.beginPacket(broadcast_ip, lifxPort);
	Udp.write((byte *) &header, sizeof(lifx_header)); // Treat the structures like byte arrays
	Udp.write((byte *) &payload, sizeof(payload));    // Which makes the data on wire correct
	Udp.endPacket();
}